package com.mj.eventapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventRequest {
    private String name;
    private String phoneNumber;
    private String hopeGift;
}

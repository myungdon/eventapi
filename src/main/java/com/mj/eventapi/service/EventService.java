package com.mj.eventapi.service;

import com.mj.eventapi.entity.People;
import com.mj.eventapi.model.EventRequest;
import com.mj.eventapi.repository.PeopleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class EventService {
    private final PeopleRepository peopleRepository;

    public void  setEvent(EventRequest request) {
        People people = new People();
        people.setName(request.getName());
        people.setPhoneNumber(request.getPhoneNumber());
        people.setHopeGift(request.getHopeGift());
        people.setDateCreate(LocalDateTime.now());

        peopleRepository.save(people);
    }
}

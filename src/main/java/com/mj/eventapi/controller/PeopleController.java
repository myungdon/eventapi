package com.mj.eventapi.controller;

import com.mj.eventapi.model.EventRequest;
import com.mj.eventapi.service.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutorService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/event")
public class PeopleController {
    private final EventService eventService;

    @PostMapping("/new")
    public String setEvent(@RequestBody EventRequest request) {
        eventService.setEvent(request);
        return "success";
    }
}
